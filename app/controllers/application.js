import Controller from '@ember/controller';

export default Controller.extend({
	actions: {
		randomPage() {
			let n = Math.floor(Math.random() * 6);
			this.set('result', n + 1);
		}
	}
});
