import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('pag-1', {path: '/pag1'});
  this.route('pag-2', {path: '/pag2'});
  this.route('pag-3', {path: '/pag3'});
  this.route('pag-4', {path: '/pag4'});
  this.route('pag-5', {path: '/pag5'});
  this.route('pag-6', {path: '/pag6'});
});

export default Router;
